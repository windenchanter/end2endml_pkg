# end2endML package

The end2endML Python package is designed for data analysts to do automate data analysis on tabular data using machine learning algorithms. The package implemented all the components, data preprocessing, data splitting, model selection, model fitting and model evaluation, required for defining pipelines to automatically analyze tabular data. 

### Installation

Install end2endML package by running:

```
pip install end2endML
```

on the command line of a Linux system or the Anaconda Prompt on a Windows system. If you don't have root privileges, sometimes you need to add --user after the above commands, then pip will install the package in your home directory.

If you want to install a specific version, please visit https://pypi.org/project/end2endML/ to find the exact version number. For example, you can install version 0.9.2 by `pip install end2endML==0.9.2`. 

### User guide

The user guide is available at https://end2endml.readthedocs.io/en/latest/.

