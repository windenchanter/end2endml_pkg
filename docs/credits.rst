Credits
=======

Mengzhe Wang, a manager at Alberta Health, and Dan Metes, a Senior Health System Analyst
at Alberta Health, originally had the idea of developing a machine
learning-based pipeline to do automatic data analysis on their administrative data sets. 
Yipeng, a postdoc at the Computational Psychiatry Group at the University of 
Alberta, volunteered to implement the machine learning parts 
of the pipeline. Then we found the pipeline is very useful to do fast data analysis in 
our research projects. Thus, we decided to implement a Python package for it. 
The package was implemented by Yipeng. The features of the Python package are the 
results of multiple discussions between Yipeng, Mengzhe and Dan.  

The project is supported by Dr. Bo Cao, PI of the Computational Psychiatry Lab at 
the University of Alberta (www.ualberta.ca/medicine/about/people/details.html?n=bo-cao), 
and by the Canada Research Chairs (CRC) program, Alberta Innovates, 
the Alberta Synergies in Alzheimer’s and Related Disorders (SynAD) program, Simon & Martina 
Sochatsky Fund for Mental Health, University Hospital Foundation, Mental Health Foundation.
