.. end2endML documentation master file, created by
   sphinx-quickstart on Fri Aug 13 23:29:01 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to end2endML's documentation!
=====================================
The end2endML Python package implemented all the components, data
preprocessing, data splitting, model selection, model fitting and model
evaluation, required for defining pipelines to do do automate data
analysis using some most commonly used machine learning algorithms. 


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   getting_started
   user_guide
   testing
   credits


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
