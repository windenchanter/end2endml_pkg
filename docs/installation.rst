Installation
============

Requirements
------------

The end2endML package requires the following dependencies:

.. code:: 

   numpy>=1.19
   pandas>=1.1
   imbalanced-learn>=0.8.0
   scikit-learn>=0.24
   pandas-profiling>=2.9.0
   joblib>=1.0
   xgboost>=1.4
   optuna>=2.7


Install
-------

It is recommended to use a virtual environment to install the package and its dependencies even though this step is optional. You can use ``conda create --name your_env_name`` to create a virtual environment if Anaconda has already been installed on your computer. Then use ``conda activate your_env_name`` to activate the virtual environment. A quick introduction of using conda to manage the virtual environment can be found at https://codingfordata.com/8-essential-commands-to-get-started-with-conda-environments/.

The package can be installed by running: ``pip install end2endML`` on the terminal of a Linux system or the Anaconda Prompt on a Windows system. If you don't have root privileges, sometimes you need to add ``--user`` after the above commands, then pip will install the package in your home directory.

If you installed the package before, you can use ``pip install end2endML --upgrade`` to update the package to the latest version. You can check the installed package version using ``import end2endML; print(end2endML.__version__)``. If you encounter a problem, you can also uninstall the package using ``pip uninstall end2endML``, then reinstall the package again.
