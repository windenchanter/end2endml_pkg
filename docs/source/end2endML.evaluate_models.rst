end2endML.evaluate\_models package
==================================

Submodules
----------

end2endML.evaluate\_models.classification\_metrics module
---------------------------------------------------------

.. automodule:: end2endML.evaluate_models.classification_metrics
   :members:
   :undoc-members:
   :show-inheritance:

end2endML.evaluate\_models.regression\_metrics module
-----------------------------------------------------

.. automodule:: end2endML.evaluate_models.regression_metrics
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: end2endML.evaluate_models
   :members:
   :undoc-members:
   :show-inheritance:
