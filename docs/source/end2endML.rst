end2endML package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   end2endML.evaluate_models
   end2endML.select_fit_models

Submodules
----------

end2endML.automate\_analysis module
-----------------------------------

.. automodule:: end2endML.automate_analysis
   :members:
   :undoc-members:
   :show-inheritance:

end2endML.automate\_modeling\_evaluation module
-----------------------------------------------

.. automodule:: end2endML.automate_modeling_evaluation
   :members:
   :undoc-members:
   :show-inheritance:

end2endML.preprocessing module
------------------------------

.. automodule:: end2endML.preprocessing
   :members:
   :undoc-members:
   :show-inheritance:

end2endML.utilities module
--------------------------

.. automodule:: end2endML.utilities
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: end2endML
   :members:
   :undoc-members:
   :show-inheritance:
