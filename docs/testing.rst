Testing
=======

The testing module is not included as part of the end2endML package. In order to 
do testing, you have to clone the whole end2endML project at https://gitlab.com/YipengUva/end2endml_pkg. 
Then install the nose2 package using ``pip install nose2``. Set the cloned project as 
the working directory and type ``nose2`` in the command line to do the testing. 
