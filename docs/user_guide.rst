User Guide
==========

A general introduction
----------------------

The end2endML package implemented all the components, data
preprocessing, data splitting, model selection, model fitting and model
evaluation, required for defining pipelines to do automate data
analysis using some most commonly used machine learning algorithms. Some
of the key components are summarized as follows.

Read and summarize the raw data
-------------------------------

Using the following script to check the arguments to control the data 
loading and data summarization behaviours.

.. code:: 

    import end2endML.utilities as utilities
    help(utilities.read_data)
    help(utilities.summarize_data)

**Key points**

* Only csv and sas7bdat format data sets are allowed.
* The data set is supposed to only contains features X and outcome y.
* A html report will be generated in working_directory/results to summarize the data.

Data splitting
--------------

.. code:: 
    
    help(utilities.split_data)

**Key points**

* When test_size == 0, K-Fold CV will be used to evaluate the selected models.
* When test_size > 0, test size will be used to evaluate the selected models. In addition, K-Fold CV can also be used.

Data preprocessing
------------------

Using the following script to check the arguments to control the behaviour of the data preprocessing process.

.. code::

    from end2endML.preprocessing import data_preprocessing
    help(data_preprocessing)

**Key points**

* All the samples with missing values at the outcome are removed.
* All the features with unique values less than a threshold, e.g., 15, are taken as categorical variables.
* The columns contain strings and over 15 unique values are taken as text data, and will be dropped.
* Samples and features with more than a threshold (e.g., 0.5) missing values are dropped.
* Categorical variables are one-hot coded with missing value as a new level and the first column is dropped.
* Median imputation is used to tackle the missing values in quantitative variables.
* Variables with a single unique value are dropped.
* The data preprocessing steps are saved and can be used for applying the same preprocessing steps on future test set.

Select and fit the models
-------------------------

Using the following script to check the arguments to control the behaviour of automate model selection and fitting.

.. code::

    from end2endML.automate_modeling_evaluation import automate_modeling
    help(automate_modeling)

**Key points**

* Depends on the data type of outcome y and its unique values, the
  modelling will be classified as the following tasks, regression,
  binary and multiclass classification.
* The following models, standard linear model (linear), linear model
  with lasso penalty (lasso), linear model with ridge penalty (ridge);
  linear model with ElasticNet penalty (elasticNet), support vector
  machine (svm), neural network (nn), gradient boosting (gb), random
  forest (rf) are installed for all the above three tasks.
* When imbalances is high, majority category is over 10 times of the
  minority category, ensemble based imbalanced learning models, balanced 
  random forest model, Random under-sampling integrated in the learning 
  of AdaBoost and Bag of balanced boosted learners, are used to model the data.
* For lasso, ridge and elasticNet, the model selection is based on the 
  model selection procedures provided by sklearn.
* For svm, nn, gb and rf, Bayesian optimization is used to do the model 
  selection. The evaluation is based on either K-Fold CV or the performance 
  on the validation set.

Evaluate the selected models using multiple metrics
---------------------------------------------------

**key points**

* The selected model can be evaluated on the test set and (or) through K-Fold CV.
* For the classification problem, these metrics, sensitivity, specificity, balanced_accuracy, recall, precision, f1_score and AUC, are used.
* For regression problem, these metrics, :math:`R^2`, mean squared error (MSE) and mean absolute error (MAE), are used.

Saved data report, preprocessed data and saved models
-----------------------------------------------------

The generated data summarization report, preprocessed data sets, saved data
preprocessing steps, saved models and saved evaluation metrics are in the results folder of the
current working directory.
